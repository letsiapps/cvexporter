﻿namespace CVExporter.Models
{
    public class Contact
    {
        public string Content { get; set; }
        public bool IsPreferred { get; set; }

        public Contact(string content, bool isPreferred = false)
        {
            Content = content;
            IsPreferred = isPreferred;
        }
    }
}