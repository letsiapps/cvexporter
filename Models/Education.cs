﻿using CVExporter.Enums;

namespace CVExporter.Models
{
    public class Education
    {
        public EducationLevel Level { get; set; }
        public string NameOfEducationalInstitution { get; set; }
        public string Department { get; set; }
        public string SpecialistField { get; set; }
        public int YearOfGraduation { get; set; }
    }
}