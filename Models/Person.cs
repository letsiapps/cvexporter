﻿using System;
using System.Collections.Generic;
using CVExporter.Enums;

namespace CVExporter.Models
{
    public class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age
        {
            get
            {
                DateTime today = DateTime.Today;
                int age = today.Year - this.BirthDate.Year;
                if (this.BirthDate > today.AddYears(-age)) age--;
                return age;
            }
        }
        public Gender Gender { get; set; }
        public string City { get; set; }
        public IList<string> Citizenship { get; set; }
        public string AdditionalInfo { get; set; }
        public IList<Contact> Contacts { get; set; }
        public IList<string> DesiredPosition { get; set; }
        public IList<Employment> Employment { get; set; }
        public IList<WorkSchedule> WorkSchedule { get; set; }
        public IList<Language> Languages { get; set; }
        public IList<string> Skills { get; set; }
        public IList<WorkExperience> WorkExperience { get; set; }
        public IList<Education> Education { get; set; }
    }
}