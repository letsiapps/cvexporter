﻿using System;

namespace CVExporter.Models
{
    public class WorkExperience
    {
        public string Organization { get; set; }
        public string Region { get; set; }
        public string Position { get; set; }
        public DateTime StartOfWork { get; set; }
        public DateTime? EndOfWork { get; set; }
        public string AdditionalInfo { get; set; }
    }
}