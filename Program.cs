﻿using System;
using System.Collections.Generic;
using CVExporter.Enums;
using CVExporter.Exporters;
using CVExporter.Models;

namespace CVExporter
{
    internal class Program
    {
        public static void Main()
        {
            Person i = new Person
            {
                Name = "Евгений",
                Surname = "Сударкин",
                BirthDate = new DateTime(1996, 9, 23),
                Gender = Gender.Male,
                City = "Ярославль",
                Citizenship = new List<string> { "Россия" },
                AdditionalInfo = "Не готов к переезду, не готов к командировкам",
                Contacts = new List<Contact>
                {
                    new Contact("+7 (910) 8208545"),
                    new Contact("sudevgeny@ya.ru", true)
                },
                DesiredPosition = new List<string> { "Младший разработчик C# / ASP.NET", "Младший разработчик Javascript" },
                Employment = new List<Employment> { Employment.PartTime, Employment.WorkPlacement },
                WorkSchedule = new List<WorkSchedule> { WorkSchedule.FlexibleSchedule },
                WorkExperience = null,
                Education = new List<Education>
                {
                    new Education
                    {
                        Level = EducationLevel.IncompleteHigher,
                        NameOfEducationalInstitution = "Ярославский государственный университет им. П.Г. Демидова",
                        Department = "Математический",
                        SpecialistField = " Компьютерная безопасность",
                        YearOfGraduation = 2020
                    }
                },
                Languages = new List<Language>
                {
                    new Language("Русский", "родной"),
                    new Language("Английский", "Pre-Intermediate (A2)")
                },
                Skills = new List<string>
                {
                    "C#", "XAML", "JavaScript", "HTML5", "CSS3", "jQuery", "MySQL", "Git", "ООП"
                }
            };

            IExporter pdfExporter = new PdfExporter();
            pdfExporter.Export(i, "CV.pdf");

            System.Diagnostics.Process.Start("explorer", AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}