﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace CVExporter.Utilities
{
    public static class Extensions
    {
        //http://stackoverflow.com/questions/1415140/can-my-enums-have-friendly-names
        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name == null) return null;
            FieldInfo field = type.GetField(name);
            if (field == null) return null;
            DescriptionAttribute attr = Attribute.GetCustomAttribute(field, 
                typeof(DescriptionAttribute)) as DescriptionAttribute;
            return attr?.Description;
        }
    }
}