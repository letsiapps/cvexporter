﻿using System.ComponentModel;

namespace CVExporter.Enums
{
    public enum EducationLevel
    {
        [Description("Не указано")]
        NotSet,
        [Description("Высшее")]
        Higher,
        [Description("Бакалавр")]
        Bachelor,
        [Description("Магистр")]
        Master,
        [Description("Кандидат наук")]
        CandidateOfSciences,
        [Description("Доктор наук")]
        DoctorOfSciences,
        [Description("Неоконченное высшее")]
        IncompleteHigher,
        [Description("Среднее специальное")]
        VocationalSecondary,
        [Description("Среднее")]
        Secondary
    }
}