﻿using System.ComponentModel;

namespace CVExporter.Enums
{
    public enum WorkSchedule
    {
        [Description("Не указано")]
        NotSet,
        [Description("Полный день")]
        FullDay,
        [Description("Сменный график")]
        ShiftSchedule,
        [Description("Гибкий график")]
        FlexibleSchedule,
        [Description("Удаленная работа")]
        RemoteWorking,
        [Description("Вахтовый метод")]
        RotationBasedWork
    }
}