﻿using System.ComponentModel;

namespace CVExporter.Enums
{
    public enum Gender
    {
        [Description("Не указано")]
        NotSet,
        [Description("Мужчина")]
        Male,
        [Description("Женщина")]
        Female
    }
}