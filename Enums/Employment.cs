﻿using System.ComponentModel;

namespace CVExporter.Enums
{
    public enum Employment
    {
        [Description("Не указано")]
        NotSet,
        [Description("Полная занятость")]
        FullTime,
        [Description("Частичная занятость")]
        PartTime,
        [Description("Проектная работа")]
        ProjectWork,
        [Description("Волонтерство")]
        Volunteering,
        [Description("Стажировка")]
        WorkPlacement
    }
}