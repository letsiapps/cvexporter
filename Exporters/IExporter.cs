﻿using CVExporter.Models;

namespace CVExporter.Exporters
{
    public interface IExporter
    {
        void Export(Person person, string fileName);
    }
}