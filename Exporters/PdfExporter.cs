﻿using System.Globalization;
using System.IO;
using System.Linq;
using CVExporter.Models;
using CVExporter.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;

namespace CVExporter.Exporters
{
    public class PdfExporter : IExporter
    {
        private readonly BaseFont _baseFont = BaseFont.CreateFont(@"Resources\Fonts\Roboto-Regular.ttf", 
            BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        private readonly BaseFont _baseBoldFont = BaseFont.CreateFont(@"Resources\Fonts\Roboto-Bold.ttf",
            BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

        private readonly BaseColor _grayFontColor = new BaseColor(174, 174, 174);
        private readonly BaseColor _grayBgColor = new BaseColor(230, 230, 230);

        private const float LargeFontSize = 25;
        private const float MediumFontSize = 12;
        private const float NormalFontSize = 9;
        private const float SmallFontSize = 8;

        public void Export(Person person, string fileName)
        {
            using (Document document = new Document(PageSize.A4, 0f, 0f, 70f, 0f))
            {

                PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));
                document.Open();

                PdfPTable table = new PdfPTable(2);
                table.SetWidths(new[] {1f, 4f});

                RenderMainSection(table, person);
                RenderDesiredPositionSection(table, person);
                RenderWorkExperienceSection(table, person);
                RenderEducationSection(table, person);
                RenderSkillsSection(table, person);

                document.Add(table);

                document.Close();
            }
        }

        private void RenderMainSection(PdfPTable table, Person person)
        {
            Phrase phrase = new Phrase
            {
                new Paragraph($"{person.Surname} {person.Name}\n", 
                    new Font(_baseFont, LargeFontSize, Font.NORMAL)),
                new Paragraph("\n", new Font(_baseFont, 4, Font.NORMAL)),
                new Paragraph($"{person.Gender.GetDescription()}, {person.Age} лет, " +
                              $"родился {person.BirthDate.ToString("d MMMM yyyy", new CultureInfo("ru-RU"))}\n",
                    new Font(_baseFont, NormalFontSize, Font.NORMAL))
            };

            if (person.Contacts != null && person.Contacts.Count > 0)
            {
                foreach (Contact contact in person.Contacts)
                {
                    phrase.Add(new Paragraph($"\n{contact.Content}", 
                        new Font(_baseFont, NormalFontSize, Font.NORMAL)));
                    if (contact.IsPreferred)
                    {
                        phrase.Add(new Paragraph(" — предпочитаемый способ связи", 
                            new Font(_baseFont, NormalFontSize, Font.NORMAL, _grayFontColor)));
                    }
                }
            }

            phrase.Add(new Paragraph($"\n"));
            if (person.City != string.Empty)
            {
                phrase.Add(new Paragraph($"\nГород: {person.City}", 
                    new Font(_baseFont, NormalFontSize, Font.NORMAL)));
            }

            if (person.Citizenship != null && person.Citizenship.Count > 0)
            {
                phrase.Add(new Paragraph($"\nГражданство: {string.Join(", ", person.Citizenship)}",
                    new Font(_baseFont, NormalFontSize, Font.NORMAL)));
            }

            if (person.AdditionalInfo != string.Empty)
            {
                phrase.Add(new Paragraph($"\n{person.AdditionalInfo}",
                    new Font(_baseFont, NormalFontSize, Font.NORMAL)));
            }

            table.AddCell(
                RenderCell(phrase));
        }

        private void RenderDesiredPositionSection(PdfPTable table, Person person)
        {
            table.AddCell(
                RenderHeaderCell("Желаемая должность"));

            Phrase phrase = new Phrase();

            if (person.DesiredPosition != null && person.DesiredPosition.Count > 0)
            {
                foreach (string position in person.DesiredPosition)
                {
                    phrase.Add(new Paragraph($"{position}\n", 
                        new Font(_baseBoldFont, MediumFontSize, Font.NORMAL)));
                }
                phrase.Add(new Chunk("\n"));
            }

            if (person.Employment != null && person.Employment.Count > 0)
            {
                phrase.Add(new Paragraph($"\nЗанятость: {string.Join(", ", person.Employment.Select(x => x.GetDescription().ToLower()))}",
                    new Font(_baseFont, NormalFontSize, Font.NORMAL)));
            }

            if (person.WorkSchedule != null && person.WorkSchedule.Count > 0)
            {
                phrase.Add(new Paragraph($"\nГрафик работы: {string.Join(", ", person.WorkSchedule.Select(x => x.GetDescription().ToLower()))}",
                    new Font(_baseFont, NormalFontSize, Font.NORMAL)));
            }

            table.AddCell(
                RenderCell(phrase));
        }

        private void RenderWorkExperienceSection(PdfPTable table, Person person)
        {
            table.AddCell(
                RenderHeaderCell("Опыт работы"));

            if (person.WorkExperience == null || person.WorkExperience.Count < 1)
            {
                Phrase phrase = new Phrase
                {
                    new Paragraph("Нет опыта", new Font(_baseFont, NormalFontSize, Font.NORMAL))
                };
                table.AddCell(
                    RenderCell(phrase));
                return;
            }

            foreach (WorkExperience experience
                in person.WorkExperience.OrderByDescending(x => x.StartOfWork))
            {
                Phrase timeOfWork = new Phrase
                {
                    
                    new Paragraph(
                        $"{experience.StartOfWork.ToString("MMMM yyyy", new CultureInfo("ru-RU"))} — " +
                        $"\n{experience.EndOfWork?.ToString("MMMM yyyy", new CultureInfo("ru-RU")) ?? "настоящее время"}",
                        new Font(_baseFont, SmallFontSize, Font.NORMAL, _grayFontColor))
                };
                table.AddCell(
                    RenderCell(timeOfWork, 1));

                Phrase companyInfo = new Phrase
                {
                    new Paragraph($"{experience.Organization}",
                        new Font(_baseBoldFont, MediumFontSize, Font.NORMAL)),
                    new Paragraph($"\n{experience.Region}",
                        new Font(_baseFont, SmallFontSize, Font.NORMAL, _grayFontColor)),
                    new Paragraph($"\n{experience.Position}",
                        new Font(_baseFont, MediumFontSize, Font.NORMAL)),
                    new Paragraph($"\n{experience.AdditionalInfo}",
                        new Font(_baseFont, NormalFontSize, Font.NORMAL))
                };
                table.AddCell(
                    RenderCell(companyInfo, 1));
            }
        }

        private void RenderEducationSection(PdfPTable table, Person person)
        {
            table.AddCell(
                RenderHeaderCell("Образование"));

            if (person.Education == null || person.Education.Count < 1)
            {
                Phrase phrase = new Phrase
                {
                    new Paragraph("Нет образования", new Font(_baseFont, NormalFontSize, Font.NORMAL))
                };
                table.AddCell(
                    RenderCell(phrase));
                return;
            }

            foreach (Education education 
                in person.Education.OrderByDescending(x => x.YearOfGraduation))
            {
                Phrase level = new Phrase
                {
                    new Paragraph($"{education.Level.GetDescription()}", 
                        new Font(_baseFont, MediumFontSize, Font.NORMAL))
                };
                table.AddCell(
                    RenderCell(level));

                Phrase year = new Phrase
                {
                    new Paragraph($"{education.YearOfGraduation}",
                        new Font(_baseFont, SmallFontSize, Font.NORMAL, _grayFontColor))
                };
                table.AddCell(
                    RenderCell(year, 1));

                Phrase institutionInfo = new Phrase()
                {
                    new Paragraph($"{education.NameOfEducationalInstitution}", 
                        new Font(_baseBoldFont, MediumFontSize, Font.NORMAL)),
                    new Paragraph($"\n{education.Department}, {education.SpecialistField}", 
                        new Font(_baseFont, NormalFontSize, Font.NORMAL))
                };
                table.AddCell(
                    RenderCell(institutionInfo, 1));
            }
        }

        private void RenderSkillsSection(PdfPTable table, Person person)
        {
            table.AddCell(
                RenderHeaderCell("Ключевые навыки"));

            if ((person.Skills == null || person.Skills.Count < 1) || 
                (person.Languages == null || person.Languages.Count < 1))
            {
                Phrase phrase = new Phrase
                {
                    new Paragraph("Нет навыков", new Font(_baseFont, NormalFontSize, Font.NORMAL))
                };
                table.AddCell(
                    RenderCell(phrase));
                return;
            }

            if (person.Languages != null && person.Languages.Count > 0)
            {
                Phrase langTitle = new Phrase
                {
                    new Paragraph("Знание языков",
                        new Font(_baseFont, SmallFontSize, Font.NORMAL, _grayFontColor))
                };
                table.AddCell(
                    RenderCell(langTitle, 1));

                Phrase langInfo = new Phrase();
                foreach (Language language in person.Languages)
                {
                    langInfo.Add(new Paragraph(language.Name, 
                        new Font(_baseFont, NormalFontSize, Font.NORMAL)));
                    langInfo.Add(new Paragraph($" — {language.Level}\n", 
                        new Font(_baseFont, NormalFontSize, Font.NORMAL, _grayFontColor)));
                }
                table.AddCell(
                    RenderCell(langInfo, 1));
            }

            if (person.Skills != null && person.Skills.Count > 0)
            {
                Phrase skillsTitle = new Phrase
                {
                    new Paragraph("Навыки",
                        new Font(_baseFont, SmallFontSize, Font.NORMAL, _grayFontColor))
                };
                table.AddCell(
                    RenderCell(skillsTitle, 1));

                Phrase skillsList = new Phrase();
                foreach (string skill in person.Skills)
                {
                    skillsList.Add(
                        new Chunk(skill, new Font(_baseFont, NormalFontSize, Font.NORMAL))
                            .SetBackground(_grayBgColor));
                    skillsList.Add(" ");
                }
                table.AddCell(
                    RenderCell(skillsList, 1));
            }
        }

        private PdfPCell RenderHeaderCell(string header)
        {
            Phrase headerPhrase = new Phrase
            {
                new Chunk("\n\n"),
                new Chunk($"{header}\n", new Font(_baseFont, MediumFontSize, Font.NORMAL, _grayFontColor)),
                new Chunk(new LineSeparator(.2f, 100f, _grayFontColor, Element.ALIGN_LEFT, MediumFontSize - 2)),
                new Chunk("\n")
            };

            return RenderCell(headerPhrase);
        }

        private PdfPCell RenderCell(Phrase phrase, int colSpan = 2, int horizontalAlignment = 0, int borderWidth = 0)
        {
            return new PdfPCell()
            {
                BorderWidth = borderWidth,
                Colspan = colSpan,
                HorizontalAlignment = horizontalAlignment,
                Phrase = phrase
            };
        }
    }
}